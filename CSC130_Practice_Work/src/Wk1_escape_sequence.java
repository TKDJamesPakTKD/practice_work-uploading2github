
public class Wk1_escape_sequence
{
  
  public static void main(String[] args) {
    /**
     *
     * Pg. 40 Question 7
     *  
     *  7. Write a statement or statements that can be used in a 
     *  Java program to display the following on the screen:
     *  
     *  3
     *  2
     *  1
     *  
     *  Using Escapse Sequence, I can output the above with 1 println
     *  
     *  Guide where I learned: https://www.youtube.com/watch?v=unLFHsCecek
     **/
    
    //regular way 
    System.out.println(3);
    System.out.println(2);
    System.out.println(1);
    
    //Escapse sequence
    //How to: "\n" makes new line
    System.out.println("\n"+"\n"+333+"\n"+33+"\n"+3); 
    System.out.print(9+"\n"+8+"\n"+7+"\n"+"\n");
    
    //so this one will be 5 new lines ("\n"+"\n"+"\n"+"\n"+"\n");
    System.out.println("start"+"\n"+"\n"+"\n"+"\n"+"\n"+"finish");
    
    // Display output to console
    System.out.println("\"James Pak\"");
   //"James Pak"
    
    //Escapse sequence
    //How to: "\t" makes a tab
    System.out.println("\t"+"James Pak"+"\n");
    
    
  
  
    
    
  }
  
}
